//
//  Game.swift
//  Bulls Eye
//
//  Created by Darwin Guzmán on 8/5/18.
//  Copyright © 2018 Darwin Guzmán. All rights reserved.
//

import Foundation

class Game {
    var target = 0
    var score = 0
    var roundGame = 1
    //solo tiene get y no set ya que todos los atributos por defecto tienen get y set
    var isWinner :Bool{
        return score>100
    }
    
    func restartGame() {
        target = Int(arc4random_uniform(100))
        score = 0
        roundGame = 1
    }
    
    func play(sliderValue:Int){
        switch sliderValue {
            case target:
                score+=100
            case (target-2)...(target+2):
                score+=50
            case (target-5)...(target+5):
                score+=10
            default:
                break
        }
        roundGame += 1
        target = Int(arc4random_uniform(100))
    }
}
