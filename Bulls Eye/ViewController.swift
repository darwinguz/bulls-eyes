//
//  ViewController.swift
//  Bulls Eye
//
//  Created by Darwin Guzmán on 25/4/18.
//  Copyright © 2018 Darwin Guzmán. All rights reserved.
//

import UIKit

class ViewController: UIViewController {
    
    @IBOutlet weak var targetLabel: UILabel!
    
    @IBOutlet weak var scoreLabel: UILabel!
    
    @IBOutlet weak var roundLabel: UILabel!
    
    @IBOutlet weak var gameSlider: UISlider!
    
    let gameModel=Game()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
        gameModel.restartGame()
        setValues()
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    
    @IBAction func playButtonPressed(_ sender: Any) {
        let sliderValue=Int(gameSlider.value)
        gameModel.play(sliderValue: sliderValue)
        setValues()
    }
    
    @IBAction func infoButtonPressed(_ sender: Any) {
        
    }
    
    @IBAction func winnerButtonPressed(_ sender: Any) {
        if gameModel.score > 100 {
            performSegue(withIdentifier: "toWinnersSegue", sender: self)
        }
    }
    @IBAction func restartButtonPressed(_ sender: Any) {
        restartGame()
        setValues()
    }
    
    func restartGame(){
        gameModel.restartGame()
    }
    
    func setValues(){
        targetLabel.text = "\(gameModel.target)"
        scoreLabel.text = "\(gameModel.score)"
        roundLabel.text = "\(gameModel.roundGame)"
    }
}

